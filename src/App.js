import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {

  constructor(props){
    super(props)

    this.nama = React.createRef()
    this.alamat = React.createRef()
    this.myForm = React.createRef()

    this.state={
      title:"My CRUD Application",
      flag:0,
      index:'',
      buttonText:"Masukkan Data",
      datas: []
    }
  }

  componentDidMount(){
    this.nama.current.focus()
  }

  formSubmit = (e) => {
    e.preventDefault()
    
    let datas = this.state.datas
    let nama = this.nama.current.value
    let alamat = this.alamat.current.value

    if(this.state.flag === 0){
      let data = {
        nama, 
        alamat
      }
      datas.push(data)
    }else{
      let index = this.state.index

      datas[index].nama = nama
      datas[index].alamat = alamat
    }

    console.log("nama anda"+nama)
    console.log("alamat anda"+alamat)

    this.setState({
      datas:datas,
      flag: 0,
      buttonText:"Masukkan Data"
    })

    this.myForm.current.reset()
    this.nama.current.focus()
  }

  formRemove = (i) =>{

    let datas = this.state.datas
    datas.splice(i,1)
    this.setState({
      datas: datas
    })

    this.myForm.current.reset()
    this.nama.current.focus()

  }

  formEdit = (i) =>{
    let data = this.state.datas[i]
    this.nama.current.value = data.nama
    this.alamat.current.value = data.alamat

    this.setState({
      flag: 1,
      index: i,
      buttonText: "Edit Data"
    })
  }

  render() {

    let datas = this.state.datas

    return (
      <div className="App">
        
        <center><h2>{ this.state.title }</h2></center>

        <form ref={this.myForm} className="myForm">
          <input type="text" ref={this.nama} placeholder="Masukkan Nama" className="formField"/>
          <input type="text" ref={this.alamat} placeholder="Masukkan Alamat" className="formField"/>

          <button onClick={this.formSubmit} className="btn btn-success btn-lg btn-block">{this.state.buttonText}</button>
        </form>
        <br/>
        <br/>
        <br/>
        <table className="table table-bordered table-striped">
          <tr>
            <th className="text-center">#</th>
            <th>Nama</th>
            <th>Alamat</th>
            <th className="text-center">Aksi</th>
          </tr>

          {datas.map((data, i) =>
            <tr key={i}>
              <td className="text-center">{i+1}.</td>
              <td>{data.nama}</td>
              <td>{data.alamat}</td>
              <td className="text-center">
                <button type="button" onClick={()=>this.formEdit(i)} className="btn btn-outline-warning btn-sm">Edit</button> <button type="button" onClick={()=>this.formRemove(i)} className="btn btn-outline-danger btn-sm">Hapus</button>
              </td>
            </tr>
          )}
        </table>
        {/* <pre>
          {datas.map((data, i) =>
            <li key={i} className="myList">
              {i+1}. {data.nama}, {data.alamat}
              <button onClick={()=>this.formRemove(i)} className="btn btn-danger btn-sm">Hapus</button>
              <button onClick={()=>this.formEdit(i)} className="myListButton">Edit</button>
            </li>
          )}
        </pre> */}
      </div>
    );
  }
}

export default App;
